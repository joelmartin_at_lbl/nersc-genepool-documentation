    <div class="main-wrapper">
        
        <h1 id="page-heading">Cori for JGI</h1>
        <section><div class="Content"><p>NERSC is pleased to provide compute capacity on its flagship supercomputer, Cori, to JGI users. <span>Burst buffer, Shifter, and all other features available to Haswell Cori nodes are available via new JGI-specific "quality of service" (QOS). </span></p>
<h2>How to access JGI Cori Compute Capacity</h2>
<p><em><strong>Access to Cori compute capacity is now available to all JGI users</strong></em>. The JGI capacity is now considered to be in production use as of January 2018. As this change was only recently made, we are requesting that users give us feedback on their experience to help us through this learning period.</p>
<p>JGI staff and Genepool-authorized researchers are provided special access to Cori via two different "quality of service" or QOS.</p>
<ul><li>For jobs requiring one or more whole nodes, use "--qos=genepool".</li>
<li>For shared jobs ("slotted" jobs in UGE nomenclature), use "--qos=genepool_shared".</li>
<li>You will also need to specify the SLURM account under which the job will run (with "-A &lt;youraccount&gt;"). </li>
<li>Do not request other standard Cori QOS settings (ie debug, premium, etc). </li>
<li><span>Jobs run under the Cori "genepool" and "genepool_shared" QOS are not charged. Fairshare rules will apply to prevent a single user or project from monopolizing the resource.</span></li>
</ul><p>Note also that the JGI's Cori capacity is entirely housed on <span>standard Haswell nodes: 64 hyperthreaded cores (32 physical) and 128GB memory</span>. It is not necessary to request Haswell nodes via Slurm (for example, with "-C haswell"). KNL nodes are NOT available via the "genepool" or "genepool_shared" qos. To use KNL nodes, you must submit to one of Cori's standard queues, and use the "m342" account - though be aware that normal Cori job charging will apply.</p>
<p>For a single-slotted job, you would minimally need:</p>
<p class="code-shell">sbatch --qos=genepool_shared -A &lt;<em>youraccount&gt;</em> <em>yourscript.sh</em></p>
<p>Another example, to request an interactive session on a single node with all CPUs (and thus memory):</p>
<p class="code-shell">salloc --qos=genepool -A &lt;<em>youraccount&gt;</em> -N 1 -n 64</p>
<p>Note that '<em>youraccount</em>' in the above example is the project name you submit to, not your login account. So <em>fungalp</em>, <em>gentechp</em> etc. Unlike during early access, you should use the same project account that you used on Genepool. If you don't know what accounts you belong to, you can check with:</p>
<p><span class="code-shell">sacctmgr show associations where user=<em>&lt;username&gt;</em></span> </p>
<p> </p>
<h3>Modules and Bioinformatics Software</h3>
<p><span>Unlike Genepool, very few NERSC software modules are available for user software. We encourage users to use alternatives wherever possible, which will be more maintainable and portable to other platforms. If you have Genepool module software that is critical to migrating your workflow to Cori, please talk to a consultant.</span></p>
<p><span>Alternatives to using software modules are:</span></p>
<ul><li><span>Shifter, with the software installed in a Docker container</span></li>
<li><span>Anaconda virtual environments allow you to install software in a clean and reproducible manner. See <a href="assets/Uploads/Anaconda-tutorial-4-26-17.pptx">this tutorial for more information about using Anaconda</a> at NERSC. Note that while Anaconda started life as a python package manager, now </span>R, Perl and many other computing languages and associated modules can also be installed using it. </li>
<li>You are, as always, welcome to install and maintain any software you like on NERSC systems within your own user disk space. While scratch space is subject to <a href="[sitetree_link,id=]#http:/www.nersc.gov/users/storage-and-file-systems/file-systems/">NERSC's purge policy</a>, many groups hold a "sandbox" space which is equally performant to $BSCRATCH and shared among group members. (Running software from $HOME is strongly discouraged.)</li>
</ul><h2>Cori Features and Other Things to Know</h2>
<p>Cori offers many additional useful features and capabilities that will be of use to JGI researchers: </p>
<h3>SLURM</h3>
<p>Unlike Genepool, Cori uses the <a href="users/computational-systems/cori/running-jobs/batch-jobs/">SLURM</a> job scheduler, which is incompatible with UGE. We've prepared a separate page to get you started on <a href="users/computational-systems/genepool/denovo-test-cluster/">converting submission scripts and commands to SLURM</a> here, and the NERSC webpages on <a href="users/computational-systems/cori/running-jobs/slurm-at-nersc-overview/">SLURM for Cori are here</a>. Complete <a href="https://slurm.schedmd.com/">SLURM documentation is here</a>, and you may also find <a href="https://slurm.schedmd.com/rosetta.pdf">this cheatsheet</a> useful.</p>
<p>The batch queues on Cori and Denovo are not configured identically. Cori and Denovo have different capabilities and maintenance cycles, and we're still learning how best to configure these machines for the JGI workloads. We welcome feedback from you on your needs and experience. If you need to write scripts that know which machine they're running on, you can use the $NERSC_HOST environment variable to check where you are.</p>
<h3>Cori scratch</h3>
<p><a style="font-size: 13px;" href="http://www.nersc.gov/users/computational-systems/cori/file-storage-and-i-o/#toc-anchor-2">Cori scratch</a><span style="font-size: 13px;"> is a Lustre filesystem, accessible through Cori and Edison, but not on Genepool or Denovo. The $CSCRATCH environment variable will point to your Cori scratch directory. Like /projectb/scratch ($BSCRATCH), Cori scratch is purged periodically, so take care to back up your files. You can find information on how to do that on <a href="http://www.nersc.gov/users/storage-and-file-systems/hpss/">the HPSS Data Archive page.</a><br></span></p>
<p><span style="font-size: 13px;">BSCRATCH is also mounted on Cori and Edison, so you can use that if you need to see your files on all machines.</span></p>
<p><span style="font-size: 13px;">Note that the performance of the different filesystems may vary, depending partly on what your application is doing. It's worth experimenting with your data in different locations to see what gives the best results.</span></p>
<h3>The Burst Buffer</h3>
<p><a style="font-size: 13px;" href="users/computational-systems/cori/burst-buffer/">The Burst buffer</a><span style="font-size: 13px;"> is a fast filesystem optimized for applications demanding high I/O. </span>The Burst Buffer is particularly suitable for applications that perform lots of random-access I/O, or that read files more than once.</p>
<p>To access the Burst Buffer you need to add directives to your batch job to make a <em>reservation</em>. A reservation can be <em>dynamic</em> or <em>persistent</em>. A dynamic reservation lasts only as long as the job that requested it, the disk space is reclaimed once the job ends. A persistent reservation outlives the job that created it, and can be shared among many jobs.</p>
<p>Use dynamic reservations for checkpoint files, for files that will be accessed randomly (i.e. not read through in a streaming manner) or just for local scratch space. Cori batch nodes don't have local disk, unlike the Genepool batch nodes, so a dynamic reservation serves that role well.</p>
<p>Use persistent reservations to store data that is shared between jobs and heavily used, e.g. reference DBs or similar data. The data on a persistent reservation is stored with normal unix filesystem permissions, and anyone can mount your persistent reservation in their batch job, so you can use them to share heavily used data among workflows belonging to a group, not just for your own private work.</p>
<p>You can access multiple persistent reservations in a single batch job, but any batch job can have only one dynamic reservation.</p>
<p>The per-user limit on Burst Buffer space is 50 TB. If the sum of your persistent and dynamic reservations reaches that total, further jobs that require Burst Buffer space will not start until some of those reservations are removed.</p>
<p>Your consultants will be providing a persistent Burst Buffer reservation with the NCBI reference BLAST databases soon, similar to the local DBs on the Genepool batch nodes.</p>
<p>This is a simple example showing how to use the Burst Buffer. See the links below for full documentation on how to use it.</p>
<p class="code-basic">#!/bin/bash<br>#SBATCH --time=00:10:00<br>#SBATCH -N 1<br>#SBATCH --constraint haswell<br>#DW jobdw capacity=240GB access_mode=striped type=scratch<br><br>echo "My BB reservation is at $DW_JOB_STRIPED"<br>cd $DW_JOB_STRIPED<br>df -h .</p>
<p>The output from a particular run of this script is below:</p>
<p class="code-basic"><code>My BB reservation is at /var/opt/cray/dws/mounts/batch/6501112_striped_scratch/<br>Filesystem                                    Size  Used Avail Use% Mounted on<br>/var/opt/cray/dws/mounts/registrations/24301  242G   99M  242G   1% /var/opt/cray/dws/mounts/batch/6501112_striped_scratch</code></p>
<p><span style="font-size: 13px;">More information on </span><a href="http://www.nersc.gov/users/computational-systems/cori/burst-buffer/example-batch-scripts/">getting started with Burst Buffer is here</a><span style="font-size: 13px;">. There are <a href="assets/Uploads/Burst-Buffer-tutorial.pdf">slides from a training session on the Burst Buffer</a> on the <a href="users/computational-systems/genepool/genepool-training-and-tutorials/">Genepool training page.</a></span></p>
<h3>Shifter</h3>
<p><a style="font-size: 13px;" href="research-and-development/user-defined-images/">Shifter</a><span style="font-size: 13px;"> is Docker-like software for running containers on NERSC systems. </span><a style="font-size: 13px;" href="users/software/using-shifter-and-docker/using-shifter-at-nersc/">More information on Shifter is available here</a><span style="font-size: 13px;">, and Shifter </span><a style="font-size: 13px;" href="assets/Uploads/Docker-for-reproducible-pipelines.pptx">training slides are also available</a><span style="font-size: 13px;">. The main advantages of using Shifter are:<br></span></p>
<ul><li><span style="font-size: 13px;">Using containers makes your workflow portable, across Cori, Denovo, Edison, and to cloud resources</span></li>
<li><span style="font-size: 13px;">You no longer need to depend on system features, such as specific compiler versions, software libraries or other tools</span></li>
<li><span style="font-size: 13px;">Because Shifter uses Docker containers, you can build and debug containers on your laptop or desktop, then be sure they will run the same way on Cori or other NERSC platforms.</span></li>
</ul><p><span style="font-size: 13px;">Shifter exists because Docker cannot be safely run on NERSC machines. Docker requires too much access to the system, anyone who can run a container can essentially access the entire machine. Shifter implements a subset of the same functionality that Docker provides, and can run Docker containers unmodified. The process for building a container and running it with Shifter is roughly as follows:</span></p>
<ul><li><span style="font-size: 13px;">use Docker on a laptop or desktop machine to build a Docker container for your software</span></li>
<li><span style="font-size: 13px;">push that container to Dockerhub or another Docker container registry</span></li>
<li><span style="font-size: 13px;">use Shifter on Cori, Edison or Denovo to <em>pull</em> that image to the NERSC Shifter registry</span></li>
<li><span style="font-size: 13px;">use Shifter on a batch node to run that container, and perform useful work.</span></li>
</ul><p><span style="font-size: 13px;">Note that Shifter is not available on Genepool, the kernel version there is too old to support containers. This is just one reason why we're commissioning Denovo.</span></p>
<p><span style="font-size: 13px;">The JGI has a containerization project, intended to provide standardized containers for the majority of JGI use-cases. If you need a container for a tool or pipeline, check with your consultants, you may find it's already been done for you. Documentation for the containerization project will be made available soon.</span></p>
<h3>Current Settings for Cori's JGI Partition</h3>
<table><tbody><tr><td>Job limits</td>
<td>5000 exclusive jobs, or 10000 shared jobs</td>
</tr><tr><td>Run time limits</td>
<td>72 h</td>
</tr><tr><td>Partition size</td>
<td>192 nodes</td>
</tr><tr><td>Node configuration</td>
<td><span>32-core Haswell CPUs (64 hyperthreads), 128GB memory</span></td>
</tr></tbody></table><p> </p>
<p> </p></div></section>
        
        
    </div>
</div>



            </div>
            <!--googleoff: snippet--><!--googleoff: index-->
            <div class="footer">
                
<div class="LastEdited">Last edited: 2018-01-30 10:35:53</div>

<div class="footer-wrapper">
        <div class="badges">
            <a href="http://www.lbl.gov/"><img src="themes/nersc-sass/images/berkeley-lab-badge.png" /></a>
            <a href="http://science.energy.gov/"><img src="themes/nersc-sass/images/doe-badge.png" /></a>
        </div>
        
        <ul class="footer-links">
            
                <li><a href="/users/accounts/user-accounts/acknowledge-nersc/">Acknowledge NERSC</a></li>
            
                <li><a href="/about/contact-us/">Contact us</a></li>
            
                <li><a href="http://www.lbl.gov/Disclaimers.html">Privacy and Security Notice</a></li>
            
            
        </ul>
        
</div>



            </div>
        </div>
    </div>

<script type="text/javascript" src="/framework/thirdparty/jquery/jquery.min.js?m=1462921760"></script><script type="text/javascript" src="/mysite/javascript/no-console.js?m=1486417999"></script><script type="text/javascript" src="/mysite/javascript/jquery.beautyOfCode.js?m=1486417999"></script><script type="text/javascript" src="/mysite/javascript/dataTables-1.7/media/js/jquery.dataTables.min.js?m=1486417999"></script><script type="text/javascript" src="/mysite/javascript/shadowbox-3.0.3/shadowbox.js?m=1486417999"></script><script type="text/javascript" src="/mysite/javascript/jquery.metadata.2.0/jquery.metadata.min.js?m=1486417999"></script><script type="text/javascript" src="/mysite/javascript/jquery.media.js?m=1486417999"></script><script type="text/javascript" src="/mysite/javascript/jquery.oembed.min.js?m=1486417999"></script><script type="text/javascript" src="/mysite/javascript/jquery.cycle/jquery.cycle.all.min.js?m=1486417999"></script><script type="text/javascript" src="/mysite/javascript/jquery.bookmark.package-1.4.0/jquery.bookmark.min.js?m=1486417999"></script><script type="text/javascript" src="/themes/nersc-sass/javascript/jquery-ui-1.8.7.custom/js/jquery-ui-1.8.7.custom.min.js?m=1486417982"></script><script type="text/javascript" src="/mysite/javascript/jquery.expander/jquery.expander.min.js?m=1486417999"></script><script type="text/javascript" src="/mysite/javascript/core.js?m=1486417999"></script><script type="text/javascript" src="/themes/nersc-sass/javascript/theme-core.js?m=1486417982"></script></body>
</html>
